---
- name: Add pdns group
  ansible.builtin.group:
    name: pdns
    system: true
    state: present

- name: Add pdns user
  ansible.builtin.user:
    name: pdns
    system: true
    create_home: false
    shell: /usr/sbin/nologin
    group: pdns

- name: Add PowerDNS recursor apt signing key
  ansible.builtin.get_url:
    url: "https://repo.powerdns.com/{{ powerdns_recursor_repository_key }}-pub.asc"
    dest: /etc/apt/keyrings/powerdns_recursor.asc
    owner: root
    group: root
    mode: "0644"

- name: Add PowerDNS recursor apt repository
  ansible.builtin.apt_repository:
    repo: "deb [signed-by=/etc/apt/keyrings/powerdns_recursor.asc] https://repo.powerdns.com/{{ ansible_distribution | lower }} {{ ansible_distribution_release }}-{{ (powerdns_recursor_repo_version | split('.'))[:-1] | join('') }} main"
    state: present
    filename: powerdns_recursor

- name: Configure apt preferences
  ansible.builtin.copy:
    src: apt-preferences
    dest: /etc/apt/preferences.d/pdns
    owner: root
    group: root
    mode: "0640"

- name: Install pdns-recursor
  ansible.builtin.apt:
    name: "pdns-recursor{% if powerdns_recursor_version is defined %}={{ powerdns_recursor_version }}{% endif %}"
    update_cache: true
    state: "{{ powerdns_recursor_package_state }}"
  notify:
    - Enable pdns-recursor
    - Daemon-reload
    - Restart pdns-recursor

- name: Fix perms on PowerDNS configuration
  ansible.builtin.file:
    name: "/etc/powerdns/recursor.conf"
    owner: "pdns"
    group: "pdns"
    mode: "0640"
    state: file

- name: Set forward zones
  ansible.builtin.set_fact:
    powerdns_recursor_forward_zone: "{{ powerdns_recursor_domains | product(['=127.0.0.1:5300;[::1]:5300']) | map('join') | join(',') }}"
    powerdns_recursor_forward_reverse_zone: "{{ powerdns_recursor_reverse_zones | product(['=127.0.0.1:5300;[::1]:5300']) | map('join') | join(',') }}"

- name: Set net.ipv6.route.max_size for pdns recursor
  ansible.posix.sysctl:
    name: net.ipv6.route.max_size
    value: "16384"
    state: present

- name: Create rpz directory
  ansible.builtin.file:
    name: "/etc/powerdns/rpz"
    owner: "pdns"
    group: "pdns"
    mode: "0750"
    state: directory

- name: Download RPZ files
  ansible.builtin.get_url:
    url: "{{ item }}"
    dest: "/etc/powerdns/rpz/{{ item | basename }}"
    owner: "pdns"
    group: "pdns"
    mode: "0640"
  with_items: "{{ powerdns_recursor_rpz_files }}"
  notify: Restart pdns-recursor

- name: File listing rpz to sync
  ansible.builtin.template:
    src: rpz-sync.txt.j2
    dest: /etc/powerdns/rpz-sync.txt
    owner: "pdns"
    group: "pdns"
    mode: "0640"
  notify: Restart pdns-recursor

- name: Configure PowerDNS recursor rpz lua
  ansible.builtin.template:
    src: recursor-rpz.lua.j2
    dest: /etc/powerdns/recursor-rpz.lua
    owner: "pdns"
    group: "pdns"
    mode: "0640"
  notify: Restart pdns-recursor

- name: Configure PowerDNS recursor
  ansible.builtin.template:
    src: recursor.conf.j2
    dest: /etc/powerdns/recursor.conf
    owner: "pdns"
    group: "pdns"
    mode: "0640"
  notify: Restart pdns-recursor

- name: Configure PowerDNS recursor rpz sync service
  ansible.builtin.copy:
    src: pdns-recursor-rpz-sync.service
    dest: /etc/systemd/system/pdns-recursor-rpz-sync.service
    owner: root
    group: root
    mode: "0640"

- name: Configure PowerDNS recursor rpz sync timer
  ansible.builtin.copy:
    src: pdns-recursor-rpz-sync.timer
    dest: /etc/systemd/system/pdns-recursor-rpz-sync.timer
    owner: root
    group: root
    mode: "0640"
  notify:
    - Enable pdns-recursor-rpz-sync timer
    - Restart pdns-recursor-rpz-sync timer
